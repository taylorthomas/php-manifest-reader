<?php

namespace TaylorThomas\WordPress\Tests\Unit;

use TaylorThomas\WordPress\ManifestReader;
use PHPUnit\Framework\TestCase;
use org\bovigo\vfs\vfsStream;

class ManifestReaderTest extends TestCase
{
  const ASSET_FILENAME           = 'main.js';
  const ASSET_FILENAME_WITH_HASH = 'main.0d2940a996ca386292c3.js';
  const ASSET_DIRECTORY          = 'assets';
  const ASSET_URI                = 'http://www.example.com';


  public function setUp()
  {
    $directory = [
      self::ASSET_DIRECTORY => [
        'manifest.json' => '{"main.js": "' . self::ASSET_FILENAME_WITH_HASH . '"}',
      ]
    ];
    $this->fileSystem = vfsStream::setup('root', 444, $directory);
  }


  protected function getManifestReader()
  {
    $dirname = $this->fileSystem->url() . DIRECTORY_SEPARATOR . self::ASSET_DIRECTORY;
    return new ManifestReader(self::ASSET_URI, $dirname);
  }


  public function testGetURI()
  {
    $manifestReader = self::getManifestReader();
    $expected       = 'http://www.example.com/' . self::ASSET_FILENAME_WITH_HASH;
    $actual         = $manifestReader->getURI(self::ASSET_FILENAME);
    $this->assertEquals($expected, $actual);
  }

  /**
   * @expectedException TaylorThomas\WordPress\ManifestNotFoundException
   */
  public function testGetPathThrowsManifestNotFoundExceptionWhenManifestNotFound()
  {
    $dirname = $this->fileSystem->url() . DIRECTORY_SEPARATOR . 'non-existent-directory';
    $manifestReader = new ManifestReader(self::ASSET_URI, $dirname);
    $manifestReader->getURI(self::ASSET_FILENAME);
  }

  /**
   * @expectedException TaylorThomas\WordPress\AssetNotFoundException
   */
  public function testGetPathThrowsAssetNotFoundExceptionWhenAssetNotFound()
  {
    $manifestReader = self::getManifestReader();
    $manifestReader->getURI('non-existent-asset');
  }
}
