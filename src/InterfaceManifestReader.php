<?php
namespace TaylorThomas\WordPress;

interface InterfaceManifestReader
{
  public function getURI(string $filename): string;
}
