<?php
namespace TaylorThomas\WordPress;

use stdClass;

class ManifestReader implements InterfaceManifestReader
{
  protected $uri;
  protected $dirname;
  protected $JSON;

  public function __construct(string $uri, string $dirname)
  {
    $this->uri     = $uri;
    $this->dirname = $dirname;
  }

  public function getURI(string $filename): string
  {
    return rtrim($this->uri, DIRECTORY_SEPARATOR)
      . DIRECTORY_SEPARATOR
      . ltrim($this->getAssetPath($filename), DIRECTORY_SEPARATOR);
  }

  protected function getJSON(): stdClass
  {
    if ($this->JSON === null) {
      $path = $this->getManifestPath();
      $contents   = @file_get_contents($path);
      if ($contents === false) {
        throw new ManifestNotFoundException("Cannot get file contents from '$path'");
      }
      $this->JSON = json_decode($contents);
    }
    return $this->JSON;
  }

  protected function getManifestPath(): string
  {
    return rtrim($this->dirname, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'manifest.json';
  }

  protected function getAssetPath(string $filename): string
  {
    $JSON = $this->getJSON();
    $path = $JSON->{$filename} ?? null;
    if ($path === null) {
      throw new AssetNotFoundException("Cannot find asset '$filename'");
    }
    return $path;
  }
}
